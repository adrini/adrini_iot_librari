<?php
class Adrini {

    private $host;
    private $apikey;
    private $channels;

    function __construct( $host, $channels, $apikey) {
        $this->host = $host;
        $this->apikey = $apikey;
        $this->channels = $channels;
    }

    public function send_data($data) {
        $url = $this->host."/api/send/".$this->channels;
        $headers = array(
            'apikey' => $this->apikey
        );
        $response = $this->request_adrini($url, $data, $headers);
        return $response;

    }

    public function get_by_field($limit, $field){
        $url = $this->host."/api/get/".$this->channels."/limit/".$limit;
        $headers = array(
            'apikey' => $this->apikey
        );
        $response = $this->request_adrini($url, $field, $headers);
        return $response;
    }

    public function get_by_channels($count){
        $url = $this->host."/api/get/channels/".$this->channels;
        $headers = array(
            'apikey' => $this->apikey
        );
        $data = array(
            'count' => $count
        );
        $response = $this->request_adrini($url, $data, $headers);
        return $response;
    }

    function request_adrini($url, $data, $headers){
        $data_fix = http_build_query($data);
        $ch = curl_init();
        $url_fix = $url."?".$data_fix;
        curl_setopt($ch, CURLOPT_URL, $url_fix);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
