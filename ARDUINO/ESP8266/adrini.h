#ifndef ADRINI_H
#define ADRINI_H

#include "ESP8266WiFi.h"
#include "WiFiClientSecure.h"
#include "Arduino.h"

class Adrini
{

  public:
    Adrini(const char* host, const char* channels, const char* apikey);
    const char* host;
    String apikey;
    const char* channels;
    WiFiClientSecure client;
    
    void adrini_setup_wifi(const char* ssid, const char* password);
    bool check_connection();
    String send_value(String url_params);
    String get_by_field(int count, String field_params);
    String get_by_channels(int count);
    String request(String data);
};

#endif // ADRINI_H
