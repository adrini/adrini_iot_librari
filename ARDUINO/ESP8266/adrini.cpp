#include "adrini.h"

Adrini::Adrini(const char* host, const char* channels, const char* apikey){
    this->host = host;
    this->apikey = apikey;
    this->channels = channels;
}

void Adrini::adrini_setup_wifi(const char* ssid, const char* password){
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("WiFi connected");
}

bool Adrini::check_connection(){
    if (!client.connect(host, 443)) {
        return false;
    }
    else{
        return true;
    }
}

String Adrini::send_value(String url_params){
    String url = "/api/send/";
    String post_data = "GET "+url+this->channels+"?"+url_params+" HTTP/1.0";
    return this->request(post_data);
}

String Adrini::get_by_channels(int limit){
    String url = "/api/get/"+String(this->channels)+"/limit/"+String(limit);
    String post_data = "GET "+url+" HTTP/1.0";
    return this->request(post_data);
    
}

String Adrini::get_by_field(int count, String field_params){
    String url = "/api/get/channels/"+String(this->channels)+"?count="+String(count)+"?"+field_params;
    String post_data = "GET "+url+" HTTP/1.0";
    return this->request(post_data);
}

String Adrini::request(String data){
    this->client.println(data);
    this->client.println("Content-Type: application/json");
    this->client.println("apikey: "+this->apikey);
    this->client.println("Host: api.adrini.com");
    this->client.println("Connection: close");
    this->client.println();
    if (this->client.connected()){
      String response = client.readString();
      return response;
    }
    else{
      return "Not Send";
    }
}
