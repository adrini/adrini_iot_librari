from adrini import Adrini 

apikey = "a713e0e7-9f7f-43d4-be1c-90c5fe6755a1443806722787409921"
id_channels = "443806782782898177"

# GET DATA BY CHANNELS
iot = Adrini(id_channels, apikey)
data_channels = iot.get_channels(4)
print("############################################")
print("DATA CHANNELS")
print("############################################")
print(data_channels)
print("############################################")

# SEND DATA TO CHANNEL BY ID
param_channels_send = {
    "sensor1": "0",
    "sensor2": "1",
    "sensor3": "1",
    "sensor4": "1",
    "lampu1": "100",
    "lampu2": "200",
    "lampu3": "300",
    "lampu4": "400"
}
response_send = iot.send_channels(param_channels_send)
print("############################################")
print("SEND DATA TO CHANNELS")
print("############################################")
print(response_send)
print("############################################")

# GET DATA CHANNEL BY ID
param_channels_by_sensor = {
    "field1": "sensor1",
    "field2": "sensor2",
    "field3": "sensor3",
    "field4": "sensor4",
    "field5": "lampu1",
    "field6": "lampu2",
    "field7": "lampu3",
    "field8": "lampu4",
}
data_channels_id = iot.get_channels_id(param_channels_by_sensor)
print("############################################")
print("DATA CHANNELS BY ID")
print("############################################")
print(data_channels_id)
print("############################################")



